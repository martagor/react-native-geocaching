import React from 'react';
import { View, Text } from 'react-native';

interface Props {}

const HomeScreen = (props: Props) => {
  return (
    <View>
      <Text>Home screen</Text>
    </View>
  );
};

export default HomeScreen;
